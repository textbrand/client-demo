import { Apollo } from "apollo-angular";
import { Injectable } from "@angular/core";

export class ApolloGraphql {
  @Injectable()
  apollo: Apollo;
}
