import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { NoDataTableComponent } from "../no-data-table/no-data-table.component";

@NgModule({
  declarations: [NoDataTableComponent],
  exports: [NoDataTableComponent],
  imports: [CommonModule]
})
export class SharedModule {}
