import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import {
  MatToolbarModule,
  MatButtonModule,
  MatSidenavModule,
  MatIconModule,
  MatListModule,
  MatGridListModule,
  MatCardModule,
  MatMenuModule,
  MatTableModule,
  MatProgressSpinnerModule,
  MatFormFieldModule,
  MatPaginatorModule,
  MatInputModule,
  MatTabsModule,
  MatSelectModule,
  MatStepperModule,
  MatRadioModule,
  MatChipsModule,
  MatDialogModule
} from "@angular/material";

const modules = [
  MatToolbarModule,
  MatButtonModule,
  MatSidenavModule,
  MatIconModule,
  MatListModule,
  MatGridListModule,
  MatCardModule,
  MatMenuModule,
  MatTableModule,
  MatProgressSpinnerModule,
  MatFormFieldModule,
  MatPaginatorModule,
  MatInputModule,
  MatTabsModule,
  MatSelectModule,
  MatStepperModule,
  MatRadioModule,
  MatChipsModule,
  MatDialogModule
];

@NgModule({
  imports: [...modules],
  exports: [...modules]
})
export class MaterialModule {}
