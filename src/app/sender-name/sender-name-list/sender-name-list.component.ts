import { SenderNameDTO } from "../../message/message.dto";
import { MatTableDataSource } from "@angular/material/table";
import { SenderNameService } from "./../sender-name.service";
import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-sender-name-list",
  templateUrl: "./sender-name-list.component.html",
  styleUrls: ["./sender-name-list.component.scss"]
})
export class SenderNameListComponent implements OnInit {
  displayedColumns: string[] = [
    "name",
    "status",
    "paid",
    "nextRenewal",
    "createdAt"
  ];
  dataSource: any;

  constructor(private senderNameService: SenderNameService) {
    this.senderNameService
      .setAllSenderNames()
      .subscribe(({ data, loading }) => {
        if (!loading) {
          this.dataSource = new MatTableDataSource<SenderNameDTO>(
            data.allSenderNames
          );
        }
      });
  }

  ngOnInit() {}
}
