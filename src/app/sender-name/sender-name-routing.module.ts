import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { SenderNameComponent } from "./sender-name.component";

const routes: Routes = [
  {
    path: "",
    component: SenderNameComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SenderNameRoutingModule {}
