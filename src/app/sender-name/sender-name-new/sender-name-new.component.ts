import { SenderNameService } from "./../sender-name.service";
import {
  FormGroup,
  FormControl,
  Validators,
  FormGroupDirective
} from "@angular/forms";
import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-sender-name-new",
  templateUrl: "./sender-name-new.component.html",
  styleUrls: ["./sender-name-new.component.scss"]
})
export class SenderNameNewComponent implements OnInit {
  form: FormGroup;

  constructor(private senderNameService: SenderNameService) {}

  ngOnInit() {
    this.form = new FormGroup({
      name: new FormControl("", Validators.required),
      sampleMessage: new FormControl("", Validators.required)
    });
  }

  clearForm() {
    this.form.controls.name.setValue("");
    this.form.controls.sampleMessage.setValue("");
  }

  onSubmitNewSenderName() {
    if (this.form.valid) {
      this.senderNameService.createSenderName(this.form.value);
      this.form.reset();
    }
  }
}
