export interface NewSenderName {
  name: string;
  sampleMessage: string;
}

export interface SenderName {
  id: string;
  name: string;
}
