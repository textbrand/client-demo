import gql from "graphql-tag";

export const ALL_SENDER_NAMES = gql`
  query AllSenderNames {
    allSenderNames {
      id
      name
      createdAt
      status
      paid
      nextRenewal
    }
  }
`;

export const CREATE_SENDER_NAME = gql`
  query CreateSenderName($input: NewSenderName!) {
    createSenderName(input: $input) {
      id
      name
      createdAt
      status
      paid
      nextRenewal
    }
  }
`;
