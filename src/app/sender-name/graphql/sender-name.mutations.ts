import gql from "graphql-tag";

export const CREATE_SENDER_NAME = gql`
  mutation createSenderName($input: NewSenderName!) {
    createSenderName(input: $input) {
      id
      name
      createdAt
      status
      paid
      nextRenewal
    }
  }
`;
