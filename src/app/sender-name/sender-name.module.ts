import { SharedModule } from "./../shared/shared.module";
import { FormsModule } from "@angular/forms";
import { MaterialModule } from "./../shared/material.module";
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { SenderNameRoutingModule } from "./sender-name-routing.module";
import { SenderNameComponent } from "./sender-name.component";
import { SenderNameNewComponent } from "./sender-name-new/sender-name-new.component";
import { SenderNameListComponent } from "./sender-name-list/sender-name-list.component";
import { ReactiveFormsModule } from "@angular/forms";

@NgModule({
  declarations: [
    SenderNameComponent,
    SenderNameNewComponent,
    SenderNameListComponent
  ],
  imports: [
    CommonModule,
    SenderNameRoutingModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule
  ]
})
export class SenderNameModule {}
