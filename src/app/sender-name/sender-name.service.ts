import { Apollo } from "apollo-angular";
import { Injectable } from "@angular/core";
import { ToastrService } from "ngx-toastr";
import * as QUERY from "./graphql/sender-name.queries";
import * as MUTATION from "./graphql/sender-name.mutations";
import { NewSenderName, SenderName } from "./sender-name.dto";

@Injectable({
  providedIn: "root"
})
export class SenderNameService {
  constructor(private toastr: ToastrService, private apollo: Apollo) {}

  setAllSenderNames() {
    return this.apollo.watchQuery<any>({ query: QUERY.ALL_SENDER_NAMES })
      .valueChanges;
  }

  createSenderName(input: NewSenderName) {
    this.apollo
      .mutate({
        mutation: MUTATION.CREATE_SENDER_NAME,
        variables: { input },
        update: async (proxy, { data: { createSenderName } }) => {
          const data: SenderName[] = proxy.readQuery({
            query: QUERY.ALL_SENDER_NAMES
          });

          data["allSenderNames"].push(createSenderName);
          proxy.writeQuery({ query: QUERY.ALL_SENDER_NAMES, data });
        }
      })
      .subscribe(({ data }) => {
        if (data["createSenderName"]) {
          this.toastr.success(
            "Sender name created but still needs verification."
          );
        }
      });
  }
}
