import { CreditsService } from "./../credits.service";
import { Component, OnInit, ElementRef } from "@angular/core";
import { FormGroup, Validators, FormControl } from "@angular/forms";
import * as credit from "../payment-details";
import { ToastrService } from "ngx-toastr";

@Component({
  selector: "app-credits-buy",
  templateUrl: "./credits-buy.component.html",
  styleUrls: ["./credits-buy.component.scss"]
})
export class CreditsBuyComponent implements OnInit {
  pricePerCredit = 2;
  paymentMethods = credit.paymentMethodsData;
  identifiers = credit.identifiersData;
  form: FormGroup;

  constructor(
    private toastr: ToastrService,
    private creditsService: CreditsService
  ) {
    this.form = new FormGroup({
      amount: new FormControl("", Validators.required),
      totalPrice: new FormControl("", Validators.required),
      method: new FormControl("", Validators.required),
      identifier: new FormControl("", Validators.required)
    });
  }

  ngOnInit() {}

  onSubmitPurchase() {
    if (this.form.valid) {
      this.creditsService.createCredit(this.form.value);
      this.form.reset();
    }
  }

  setIdentifier(identifier: string) {
    this.form.patchValue({ identifier });
  }

  setTotalPrice(amount) {
    this.form.patchValue({ totalPrice: amount * this.pricePerCredit });
  }

  copyText(val: string) {
    if (!val) {
      return;
    }

    const selBox = document.createElement("textarea");
    selBox.style.position = "fixed";
    selBox.style.left = "0";
    selBox.style.top = "0";
    selBox.style.opacity = "0";
    selBox.value = val;
    document.body.appendChild(selBox);
    selBox.focus();
    selBox.select();
    document.execCommand("copy");
    document.body.removeChild(selBox);
    this.toastr.info(`${val} is Copied.`);
  }
}
