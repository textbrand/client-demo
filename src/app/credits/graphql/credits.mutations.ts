import gql from "graphql-tag";

export const CREATE_CREDIT = gql`
  mutation CreateCredit($input: NewCredit!) {
    createCredit(input: $input) {
      id
      userName
      amount
      totalPrice
      method
      identifier
      status
      createdAt
    }
  }
`;

export const DELETE_CREDIT = gql`
  mutation DeleteCredit($creditId: String!) {
    deleteCredit(creditId: $creditId)
  }
`;

export const UPDATE_CREDIT = gql`
  mutation UpdateCredit($creditId: String!, $input: UpdateCredit!) {
    updateCredit(creditId: $creditId, input: $input) {
      id
      userName
      amount
      totalPrice
      method
      identifier
      status
      createdAt
    }
  }
`;
