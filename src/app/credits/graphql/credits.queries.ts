import gql from "graphql-tag";

export const ALL_CREDITS = gql`
  query AllCredits {
    allCredits {
      id
      userName
      amount
      totalPrice
      method
      identifier
      status
      createdAt
    }

    totalAvailableCredits
  }
`;
