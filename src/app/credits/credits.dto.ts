export enum CreditStatus {
  Pending = "Pending",
  Declined = "Declined",
  Approved = "Approved"
}

export enum CreditMethod {
  "GCash",
  "Coins.ph",
  "BPI - Brank Transfer",
  "UnionBrank - Bank Transfer"
}

export interface CreditDTO {
  id?: string;
  userName?: string;
  amount: number;
  totalPrice: number;
  method: CreditMethod;
  identifier: string;
  status?: CreditStatus;
  createdAt?: string;
}
