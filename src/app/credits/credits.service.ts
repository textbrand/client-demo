import { Apollo } from "apollo-angular";
import { Injectable } from "@angular/core";
import { ToastrService } from "ngx-toastr";
import * as QUERY from "./graphql/credits.queries";
import * as MUTATION from "./graphql/credits.mutations";
import { CreditDTO } from "./credits.dto";

@Injectable({
  providedIn: "root"
})
export class CreditsService {
  constructor(private toastr: ToastrService, private apollo: Apollo) {}

  setAllCredits() {
    return this.apollo.watchQuery<any>({ query: QUERY.ALL_CREDITS })
      .valueChanges;
  }

  createCredit(input: CreditDTO) {
    this.apollo
      .mutate({
        mutation: MUTATION.CREATE_CREDIT,
        variables: { input },
        update: async (proxy, { data: { createCredit } }) => {
          const data: CreditDTO[] = proxy.readQuery({
            query: QUERY.ALL_CREDITS
          });

          data["allCredits"].push(createCredit);
          proxy.writeQuery({ query: QUERY.ALL_CREDITS, data });
        }
      })
      .subscribe(({ data }) => {
        if (data["createCredit"]) {
          this.toastr.success("Credit is added for approval.");
        }
      });
  }
}
