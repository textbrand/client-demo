export interface PaymentMethod {
  value: string;
  label: string;
}

export interface IdentifierDetail {
  label: string;
  detail: string;
}

export const paymentMethodsData: PaymentMethod[] = [
  { value: "0", label: "GCash" },
  { value: "1", label: "Coins.ph" },
  { value: "2", label: "BPI - Brank Transfer" },
  { value: "3", label: "UnionBrank - Bank Transfer" }
];

export const identifiersData: IdentifierDetail[] = [
  {
    label: "Mobile Number",
    detail: "09953263583"
  },
  {
    label: "Peso Wallet Address",
    detail: "32f3ZXeryfgm8LQoTvp3Ccj42g6yFeKaor"
  },
  {
    label: "Account Number (Joseph Getaruelas)",
    detail: "1199215686"
  },
  {
    label: "Account Number (Joseph Getaruelas)",
    detail: "100578047863"
  }
];
