import { CreditDTO } from "./../credits.dto";
import { MatTableDataSource } from "@angular/material/table";
import { CreditsService } from "./../credits.service";
import { Component, OnInit, ViewChild } from "@angular/core";
import { paymentMethodsData } from "../payment-details";

@Component({
  selector: "app-credits-buy-list",
  templateUrl: "./credits-buy-list.component.html",
  styleUrls: ["./credits-buy-list.component.scss"]
})
export class CreditsBuyListComponent implements OnInit {
  paymentMethod = [];
  displayedColumns: string[] = [
    "userName",
    "amount",
    "totalPrice",
    "method",
    "identifier",
    "status",
    "createdAt"
  ];

  dataSource;

  constructor(private creditsService: CreditsService) {
    this.setPaymentMethod();
  }

  ngOnInit() {
    this.creditsService.setAllCredits().subscribe(({ data, loading }) => {
      if (!loading) {
        this.dataSource = new MatTableDataSource<CreditDTO>(data.allCredits);
      } else {
        this.dataSource = new MatTableDataSource<CreditDTO>([]);
      }
    });
  }

  setPaymentMethod() {
    paymentMethodsData.forEach(pm => {
      this.paymentMethod[pm.value] = pm.label;
    });
  }
}
