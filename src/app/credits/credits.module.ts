import { SharedModule } from "./../shared/shared.module";
import { MaterialModule } from "./../shared/material.module";
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";

import { CreditsRoutingModule } from "./credits-routing.module";
import { CreditsComponent } from "./credits.component";
import { CreditsBuyComponent } from "./credits-buy/credits-buy.component";
import { CreditsBuyListComponent } from "./credits-buy-list/credits-buy-list.component";

@NgModule({
  declarations: [
    CreditsComponent,
    CreditsBuyComponent,
    CreditsBuyListComponent
  ],
  imports: [
    CommonModule,
    CreditsRoutingModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule
  ]
})
export class CreditsModule {}
