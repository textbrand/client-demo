import { AuthService } from "./auth/auth.service";
import { environment } from "./../environments/environment";
import { NgModule } from "@angular/core";
import { ApolloModule, APOLLO_OPTIONS, Apollo } from "apollo-angular";
import { HttpLinkModule, HttpLink } from "apollo-angular-link-http";
import { InMemoryCache } from "apollo-cache-inmemory";
import { ToastrService } from "ngx-toastr";
import { setContext } from "apollo-link-context";
import { onError } from "apollo-link-error";
import { ApolloLink } from "apollo-link";

const uri = environment.graphqlUri; // <-- add the URL of the GraphQL server here
export function createApollo(httpLink: HttpLink) {
  return {
    link: httpLink.create({ uri }),
    cache: new InMemoryCache()
  };
}

@NgModule({
  exports: [ApolloModule, HttpLinkModule],
  providers: []
})
export class GraphQLModule {
  constructor(
    apollo: Apollo,
    httpLink: HttpLink,
    private authService: AuthService
  ) {
    const http = httpLink.create({ uri });

    const auth = setContext((_, { headers }) => {
      const token = localStorage.getItem("JWT_TOKEN");

      if (!token) {
        return {};
      } else {
        return {
          headers: { Authorization: `Bearer ${token}`, ...headers }
        };
      }
    });

    const errorLink = onError(({ graphQLErrors, networkError }) => {
      if (graphQLErrors) {
        graphQLErrors.map(({ message, locations, path }) => {
          if (message.statusCode === 401) {
            if (this.authService.isLoggedIn()) {
              this.authService.doLogout(true);
            }
          }
        });
      }

      if (networkError) {
        console.log(`[Network error]: ${networkError}`);
      }
    });

    const httpLinkWithErrorHandling = ApolloLink.from([errorLink, auth, http]);

    apollo.create({
      link: httpLinkWithErrorHandling,
      cache: new InMemoryCache(),
      defaultOptions: {
        watchQuery: {
          fetchPolicy: "cache-and-network",
          errorPolicy: "ignore"
        },
        query: {
          fetchPolicy: "network-only",
          errorPolicy: "all"
        },
        mutate: {
          errorPolicy: "all"
        }
      }
    });
  }
}
