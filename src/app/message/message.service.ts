import { NewMessage } from "./message.dto";
import { Apollo } from "apollo-angular";
import { Injectable } from "@angular/core";
import { ToastrService } from "ngx-toastr";
import * as QUERY from "./graphql/messages.queries";
import * as MUTATION from "./graphql/messages.mutations";

@Injectable({
  providedIn: "root"
})
export class MessageService {
  constructor(private toastr: ToastrService, private apollo: Apollo) {}

  sendMessage(input: NewMessage) {
    this.apollo
      .mutate({
        mutation: MUTATION.SEND_MESSAGE,
        variables: { input },
        refetchQueries: [{ query: QUERY.ALL_SENT_MESSAGES }]
      })
      .subscribe(({ data }) => {
        const result = data["sendMessage"];
        if (result) {
          if (result.status) {
            this.toastr.success(result.message);
          } else {
            this.toastr.error(result.message);
          }
        } else {
          this.toastr.warning("Something went wrong");
        }
      });
  }

  setAllMessages() {
    return this.apollo.watchQuery<any>({ query: QUERY.ALL_SENT_MESSAGES })
      .valueChanges;
  }
}
