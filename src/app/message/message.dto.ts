export interface MessageTemplate {
  id: string;
  name: string;
  message: string;
}

export interface NewMessage {
  senderNameId: string;
  receiverNo: string[];
  message: string;
}

export interface Message {
  senderName: string;
  mobileNo: string;
  message: string;
  scheduled: boolean;
  status: string;
  date: string;
}

export interface SenderNameDTO {
  name: string;
  status: string;
  paid: boolean;
  nextRenewal: string;
  created: string;
}
