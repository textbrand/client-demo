import gql from "graphql-tag";

export const DELETE_MESSAGE_TEMPLATE = gql`
  mutation DeleteMessageTemplate($messageTemplateId: String!) {
    deleteMessageTemplate(messageTemplateId: $messageTemplateId)
  }
`;

export const CREATE_MESSAGE_TEMPLATE = gql`
  mutation CreateMessageTemplate($input: NewMessageTemplate!) {
    createMessageTemplate(input: $input) {
      id
      name
      message
    }
  }
`;

export const UPDATE_MESSAGE_TEMPLATE = gql`
  mutation UpdateMessageTemplate(
    $messageTemplateId: String!
    $input: UpdateMessageTemplate!
  ) {
    updateMessageTemplate(
      messageTemplateId: $messageTemplateId
      input: $input
    ) {
      id
      name
      message
    }
  }
`;

export const SEND_MESSAGE = gql`
  mutation SendMessage($input: Message!) {
    sendMessage(input: $input) {
      status
      message
    }
  }
`;
