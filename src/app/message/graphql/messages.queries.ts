import gql from "graphql-tag";

export const ALL_MESSAGE_TEMPLATES = gql`
  query AllMessageTemplates {
    allMessageTemplates {
      id
      name
      message
    }
  }
`;

export const ALL_SENT_MESSAGES = gql`
  query AllSentMessages {
    allSentMessages {
      id
      senderNameUsed
      receiverNo
      message
      createdAt
      status
    }
  }
`;
