import { MessageService } from "./../message.service";
import { Message } from "./../message.dto";
import { Component, OnInit, ViewChild } from "@angular/core";
import { MatPaginator } from "@angular/material/paginator";
import { MatSort } from "@angular/material/sort";
import { MatTableDataSource } from "@angular/material/table";

@Component({
  selector: "app-message-list",
  styleUrls: ["message-list.component.scss"],
  templateUrl: "message-list.component.html"
})
export class MessageListComponent implements OnInit {
  displayedColumns: string[] = [
    "senderNameUsed",
    "receiverNo",
    "message",
    "createdAt",
    "status"
  ];
  dataSource;

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  constructor(private messageService: MessageService) {
    this.messageService.setAllMessages().subscribe(({ data, loading }) => {
      if (!loading) {
        this.dataSource = new MatTableDataSource<Message>(data.allSentMessages);
      } else {
        this.dataSource = new MatTableDataSource<Message>([]);
      }
    });
  }

  ngOnInit() {
    this.dataSource.paginator = this.paginator;
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
}
