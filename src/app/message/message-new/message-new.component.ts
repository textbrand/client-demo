import { MessageService } from "./../message.service";
import { SenderNameService } from "./../../sender-name/sender-name.service";
import { Component, OnInit } from "@angular/core";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { SenderName } from "../../sender-name/sender-name.dto";
import { MessageTemplate } from "../message.dto";
import { MessageTemplateService } from "../message-template/message-template.service";

const mobileNoValidtor = (control: FormControl) => {
  const mobileNumbers = control.value.split(",");

  for (const mobileNumer of mobileNumbers) {
    const mobileNoPrefix = mobileNumer.trim().slice(0, 2);
    const mobileNoLength = mobileNumer.trim().length === 12;

    if (!(mobileNoPrefix && mobileNoLength)) {
      return { invalidNumber: true };
    }
  }

  return null;
};

@Component({
  selector: "app-message-new",
  templateUrl: "./message-new.component.html",
  styleUrls: ["./message-new.component.scss"]
})
export class MessageNewComponent implements OnInit {
  form: FormGroup;
  senderNames: SenderName[] = [];
  messageTemplates: MessageTemplate[];
  messageTemplate: string = "";

  constructor(
    private senderNameService: SenderNameService,
    private messageService: MessageService,
    private messageTemplateService: MessageTemplateService
  ) {}

  ngOnInit() {
    this.setSenderNames();
    this.initializeForm();
    this.setMessageTemplate();
  }

  initializeForm() {
    this.form = new FormGroup({
      senderNameId: new FormControl("", Validators.required),
      to: new FormControl("", [Validators.required, mobileNoValidtor]),
      message: new FormControl("", Validators.required)
    });
  }

  setMessageTemplate() {
    this.messageTemplateService
      .setAllMessageTemplates()
      .subscribe(({ data, loading }) => {
        if (!loading) {
          this.messageTemplates = [
            { name: "None", message: "" },
            ...data.allMessageTemplates
          ];
        }
      });
  }

  setSenderNames() {
    this.senderNameService
      .setAllSenderNames()
      .subscribe(({ data, loading }) => {
        if (!loading) {
          this.senderNames = data.allSenderNames || [];
          this.senderNames = this.senderNames.filter(
            senderName => senderName["status"] === "Active"
          );

          if (this.senderNames.length === 0) {
            this.senderNames.push({ id: "0", name: "SysDefault" });
          }

          this.form.controls.senderNameId.setValue(this.senderNames[0].id);
        }
      });
  }

  toArrayMobileNumbers(numbers: string) {
    let mobileNumbersArray = [];
    for (const mobileNumber of numbers.split(",")) {
      mobileNumbersArray = [...mobileNumbersArray, mobileNumber.trim()];
    }

    return mobileNumbersArray;
  }

  onSendSMS() {
    if (this.form.valid) {
      const { senderNameId, to, message } = this.form.value;

      this.messageService.sendMessage({
        senderNameId,
        receiverNo: this.toArrayMobileNumbers(to),
        message
      });

      this.resetForm(this.form);
    }
  }

  resetForm(form: FormGroup) {
    console.log(form.controls);
    Object.keys(form.controls).forEach(key => {
      if (key !== "senderNameId") {
        form.get(key).setValue("");
      }
    });
  }

  insertMessageTemplate(value) {
    if (value) {
      this.form.controls.message.setValue(
        (this.form.controls.message.value + " " + value).trim()
      );
    }
  }
}
