import { Apollo } from "apollo-angular";
import { MessageTemplate } from "../message.dto";
import * as QUERY from "../graphql/messages.queries";
import * as MUTATION from "../graphql/messages.mutations";
import { Injectable } from "@angular/core";
import { NewTemplate } from "./message-template-new/message-template-new.component";
import { ToastrService } from "ngx-toastr";

@Injectable({
  providedIn: "root"
})
export class MessageTemplateService {
  constructor(private toastr: ToastrService, private apollo: Apollo) {}
  setAllMessageTemplates() {
    return this.apollo.watchQuery<any>({ query: QUERY.ALL_MESSAGE_TEMPLATES })
      .valueChanges;
  }

  submitNewMessageTemplate(input: NewTemplate) {
    this.apollo
      .mutate({
        mutation: MUTATION.CREATE_MESSAGE_TEMPLATE,
        variables: { input },
        update: async (proxy, { data: { createMessageTemplate } }) => {
          const data: MessageTemplate[] = proxy.readQuery({
            query: QUERY.ALL_MESSAGE_TEMPLATES
          });

          data["allMessageTemplates"].push(createMessageTemplate);
          proxy.writeQuery({ query: QUERY.ALL_MESSAGE_TEMPLATES, data });
        }
      })
      .subscribe(({ data }) => {
        if (data["createMessageTemplate"]) {
          this.toastr.success("Message template created.");
        }
      });
  }

  deleteMessageTemplate(messageTemplateId) {
    this.apollo
      .mutate<any>({
        mutation: MUTATION.DELETE_MESSAGE_TEMPLATE,
        variables: { messageTemplateId },
        update: async (proxy, { loading }) => {
          if (!loading) {
            const data = proxy.readQuery({
              query: QUERY.ALL_MESSAGE_TEMPLATES
            });
            data["allMessageTemplates"] = data["allMessageTemplates"].filter(
              template => template.id !== messageTemplateId
            );
          }
        }
      })
      .subscribe(({ data: { deleteMessageTemplate } }) => {
        if (deleteMessageTemplate) {
          this.toastr.success("Message template deleted.");
        } else {
          this.toastr.error("Unable to delete.");
        }
      });
  }

  updateMessageTemplate(messageTemplateId, input) {
    this.apollo
      .mutate<any>({
        mutation: MUTATION.UPDATE_MESSAGE_TEMPLATE,
        variables: { messageTemplateId, input }
      })
      .subscribe(({ data: { updateMessageTemplate }, loading }) => {
        if (!loading && updateMessageTemplate) {
          this.toastr.success("Message template updated.");
        } else {
          this.toastr.error("Unable to update.");
        }
      });
  }
}
