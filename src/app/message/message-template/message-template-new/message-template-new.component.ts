import { MessageTemplateService } from "../message-template.service";
import { Validators, FormControl, FormGroup } from "@angular/forms";
import { MatDialogRef } from "@angular/material";
import { Component, OnInit } from "@angular/core";

export interface NewTemplate {
  name: string;
  message: string;
}

@Component({
  selector: "app-message-template-new",
  templateUrl: "./message-template-new.component.html",
  styleUrls: ["./message-template-new.component.scss"]
})
export class MessageTemplateNewComponent implements OnInit {
  form: FormGroup;
  constructor(
    public dialogRef: MatDialogRef<MessageTemplateNewComponent>,
    private messageTemplateService: MessageTemplateService
  ) {}

  ngOnInit() {
    this.form = new FormGroup({
      name: new FormControl("", Validators.required),
      message: new FormControl("", Validators.required)
    });
  }

  onSubmitNewMessageTemplate() {
    if (!this.form.invalid) {
      this.messageTemplateService.submitNewMessageTemplate(this.form.value);
      this.dialogRef.close();
    }
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
}
