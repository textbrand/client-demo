import { MessageTemplate } from "./../../message.dto";
import { MessageTemplateService } from "../message-template.service";
import { Validators } from "@angular/forms";
import { FormControl } from "@angular/forms";
import { FormGroup } from "@angular/forms";
import { MAT_DIALOG_DATA, MatDialogRef } from "@angular/material";
import { Component, OnInit, Inject } from "@angular/core";

@Component({
  selector: "app-message-template-edit",
  templateUrl: "./message-template-edit.component.html",
  styleUrls: ["./message-template-edit.component.scss"]
})
export class MessageTemplateEditComponent implements OnInit {
  form: FormGroup;
  messageTemplatedId: string;

  constructor(
    public dialogRef: MatDialogRef<MessageTemplateEditComponent>,
    @Inject(MAT_DIALOG_DATA) public paramData: MessageTemplate,
    private messageTemplateService: MessageTemplateService
  ) {
    const { name, message, id } = paramData;
    this.messageTemplatedId = id;
    this.form = new FormGroup({
      name: new FormControl(name, Validators.required),
      message: new FormControl(message, Validators.required)
    });
  }

  ngOnInit() {}

  onNoClick(): void {
    this.dialogRef.close();
  }

  onEditNewMessageTemplate() {
    if (this.form.valid) {
      this.messageTemplateService.updateMessageTemplate(
        this.messageTemplatedId,
        this.form.value
      );
      this.dialogRef.close();
    }
  }
}
