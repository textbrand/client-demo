import { MatTableDataSource } from "@angular/material/table";
import { MessageTemplate } from "./../message.dto";
import { MessageTemplateNewComponent } from "./message-template-new/message-template-new.component";
import { MessageTemplateEditComponent } from "./message-template-edit/message-template-edit.component";
import { Component, OnInit } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { MessageTemplateService } from "./message-template.service";

@Component({
  selector: "app-message-template",
  templateUrl: "./message-template.component.html",
  styleUrls: ["./message-template.component.scss"]
})
export class MessageTemplateComponent implements OnInit {
  displayedColumns: string[] = ["name", "message", "action"];
  dataSource: any;

  constructor(
    public dialog: MatDialog,
    private messageTemplateService: MessageTemplateService
  ) {}

  ngOnInit() {
    this.messageTemplateService
      .setAllMessageTemplates()
      .subscribe(({ data, loading }) => {
        if (!loading) {
          this.dataSource = new MatTableDataSource<MessageTemplate>(
            data.allMessageTemplates
          );
        }
      });
  }

  addNewTemplate() {
    const dialogRef = this.dialog.open(MessageTemplateNewComponent, {
      width: "400px"
    });
  }

  onDeleteMessageTemplate(messageTemplateId) {
    this.messageTemplateService.deleteMessageTemplate(messageTemplateId);
  }

  editDialog(element: MessageTemplate): void {
    const dialogRef = this.dialog.open(MessageTemplateEditComponent, {
      width: "400px",
      data: { ...element }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (!result) {
        return;
      }
    });
  }
}
