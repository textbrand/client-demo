import { SharedModule } from "./../shared/shared.module";
import { GraphQLModule } from "./../graphql.module";
import { MaterialModule } from "./../shared/material.module";
import { MessageListComponent } from "./message-list/message-list.component";
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";

import { MessageRoutingModule } from "./message-routing.module";
import { MessageComponent } from "./message.component";
import { MessageNewComponent } from "./message-new/message-new.component";
import { MessageTemplateComponent } from "./message-template/message-template.component";
import { MessageTemplateEditComponent } from "./message-template/message-template-edit/message-template-edit.component";
import { MessageTemplateNewComponent } from "./message-template/message-template-new/message-template-new.component";

@NgModule({
  declarations: [
    MessageComponent,
    MessageListComponent,
    MessageNewComponent,
    MessageTemplateComponent,
    MessageTemplateEditComponent,
    MessageTemplateNewComponent
  ],
  imports: [
    CommonModule,
    MessageRoutingModule,
    MaterialModule,
    GraphQLModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule
  ],
  entryComponents: [MessageTemplateEditComponent, MessageTemplateNewComponent],
  exports: [MessageListComponent]
})
export class MessageModule {}
