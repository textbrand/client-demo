import { MessageModule } from "./../message/message.module";
import { MaterialModule } from "./../shared/material.module";
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { DashboardRoutingModule } from "./dashboard-routing.module";
import { DashboardComponent } from "./dashboard.component";

@NgModule({
  declarations: [DashboardComponent],
  imports: [CommonModule, DashboardRoutingModule, MaterialModule, MessageModule]
})
export class DashboardModule {}
