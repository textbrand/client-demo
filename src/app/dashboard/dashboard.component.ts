import { CreditStatus } from "./../credits/credits.dto";
import { Component, OnInit } from "@angular/core";
import { CreditsService } from "../credits/credits.service";
import { MessageService } from "../message/message.service";

@Component({
  selector: "app-dashboard",
  templateUrl: "./dashboard.component.html",
  styleUrls: ["./dashboard.component.scss"]
})
export class DashboardComponent implements OnInit {
  totalAvailableCredits: number;
  totalSentMessages: number;
  totalUnSentMessages: number;

  constructor(
    private creditsService: CreditsService,
    private messageService: MessageService
  ) {}

  ngOnInit() {
    this.setTotalAvailableCredits();
    this.setTotalSentMessages();
    this.setTotalUnSentMessages();
  }

  setTotalSentMessages() {
    this.messageService.setAllMessages().subscribe(({ data, loading }) => {
      if (data && !loading) {
        this.totalSentMessages = data["allSentMessages"].length || 0;
      }
    });
  }

  setTotalUnSentMessages() {
    this.messageService.setAllMessages().subscribe(({ data, loading }) => {
      if (data && !loading) {
        this.totalUnSentMessages = 0;

        for (const message of data.allSentMessages) {
          this.totalUnSentMessages += message.status !== "Sent" ? 1 : 0;
        }
      }
    });
  }

  setTotalAvailableCredits() {
    this.creditsService.setAllCredits().subscribe(({ data, loading }) => {
      if (data && !loading) {
        this.totalAvailableCredits = data.totalAvailableCredits;
      }
    });
  }
}
