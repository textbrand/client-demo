import { NoAuthGuard } from "./guards/no-auth-guard";
import { CanActivateViaAuthGuard } from "./guards/can-activate-via-auth-guard";
import { BackOfficeComponent } from "./back-office/back-office.component";
import { LoginComponent } from "./login/login.component";
import { HomeComponent } from "./home/home.component";
// tslint:disable-next-line: quotemark
import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { RegisterComponent } from "./register/register.component";

// tslint:disable-next-line: quotemark
const routes: Routes = [
  {
    path: "",
    component: HomeComponent
  },
  {
    path: "login",
    component: LoginComponent,
    canActivate: [NoAuthGuard]
  },
  {
    path: "register",
    component: RegisterComponent,
    canActivate: [NoAuthGuard]
  },
  {
    path: "app",
    component: BackOfficeComponent,
    canActivate: [CanActivateViaAuthGuard],
    children: [
      {
        path: "dashboard",
        loadChildren: () =>
          import("./dashboard/dashboard.module").then(
            mod => mod.DashboardModule
          )
      },
      {
        path: "message",
        loadChildren: () =>
          import("./message/message.module").then(mod => mod.MessageModule)
      },
      {
        path: "credits",
        loadChildren: () =>
          import("./credits/credits.module").then(mod => mod.CreditsModule)
      },
      {
        path: "sender-name",
        loadChildren: () =>
          import("./sender-name/sender-name.module").then(
            mod => mod.SenderNameModule
          )
      },
      {
        path: "",
        redirectTo: "dashboard",
        pathMatch: "full"
      }
    ]
  },
  {
    path: "",
    redirectTo: "",
    pathMatch: "full"
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule {}
