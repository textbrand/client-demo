import { Observable } from "rxjs";
import { Apollo } from "apollo-angular";
import { Router } from "@angular/router";
import { Injectable } from "@angular/core";
import gql from "graphql-tag";
import { ToastrService } from "ngx-toastr";
import { Tokens } from "./token.interceptor";

const loginMutation = gql`
  mutation userLogin($login: Login) {
    userLogin(login: $login) {
      token
      user {
        fullName
      }
    }
  }
`;

export const CREATE_USER_MUTATION = gql`
  mutation createUser($input: CreateInput!) {
    createUser(input: $input) {
      firstname
      lastname
      email
      mobileNo
    }
  }
`;

@Injectable({
  providedIn: "root"
})
export class AuthService {
  private readonly JWT_TOKEN = "JWT_TOKEN";
  currentUser: any;

  constructor(
    private readonly router: Router,
    private apollo: Apollo,
    private toastr: ToastrService
  ) {}

  doLogin(login: any) {
    this.apollo
      .mutate({
        mutation: loginMutation,
        variables: { login }
      })
      .subscribe(result => {
        const data = result ? result.data : null;

        if (data && data.userLogin) {
          this.setCurrentUser(data.userLogin.user);
          this.storeTokens(data.userLogin);
          this.apollo.getClient().resetStore();
          this.router.navigateByUrl("/app/dashboard");
        } else {
          this.toastr.error("Invalid Credential");
        }
      });
  }

  setCurrentUser(user) {
    this.currentUser = user.fullName;
  }

  getCurrentUser() {
    return this.currentUser || localStorage.getItem("currentUser");
  }

  doLogout(force?: boolean) {
    if (force !== true) {
      this.toastr.success("Logout Successful");
    }
    this.router.navigateByUrl("/login");
    this.apollo.getClient().clearStore();
    localStorage.removeItem(this.JWT_TOKEN);
    localStorage.removeItem("currentUser");
  }

  isLoggedIn() {
    return !!this.getJwtToken();
  }

  getJwtToken() {
    return localStorage.getItem(this.JWT_TOKEN);
  }

  private storeTokens(tokens: Tokens) {
    localStorage.setItem(this.JWT_TOKEN, tokens.token);
    localStorage.setItem("currentUser", tokens.user.fullName);
  }

  private removeTokens() {
    localStorage.removeItem(this.JWT_TOKEN);
    localStorage.removeItem("currentUser");
  }

  registerUser(input): Observable<any> {
    return this.apollo.mutate({
      mutation: CREATE_USER_MUTATION,
      variables: { input }
    });
  }
}
