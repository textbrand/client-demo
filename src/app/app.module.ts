import { CanActivateViaAuthGuard } from "./guards/can-activate-via-auth-guard";
import { MaterialModule } from "./shared/material.module";
import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { MainNavComponent } from "./main-nav/main-nav.component";
import { LayoutModule } from "@angular/cdk/layout";
import { HomeComponent } from "./home/home.component";
import { HttpClientModule } from "@angular/common/http";
import { GraphQLModule } from "./graphql.module";

import { ToastrModule } from "ngx-toastr";
import { LoginComponent } from "./login/login.component";
import { ReactiveFormsModule, FormsModule } from "@angular/forms";
import { BackOfficeComponent } from "./back-office/back-office.component";
import { HomeHeaderComponent } from "./home-header/home-header.component";
import { RegisterComponent } from "./register/register.component";
import { NoAuthGuard } from "./guards/no-auth-guard";

@NgModule({
  declarations: [
    AppComponent,
    MainNavComponent,
    HomeComponent,
    LoginComponent,
    BackOfficeComponent,
    HomeHeaderComponent,
    RegisterComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    LayoutModule,
    MaterialModule,
    HttpClientModule,
    GraphQLModule,
    ReactiveFormsModule,
    FormsModule,
    ToastrModule.forRoot()
  ],
  providers: [CanActivateViaAuthGuard, NoAuthGuard],
  bootstrap: [AppComponent]
})
export class AppModule {}
